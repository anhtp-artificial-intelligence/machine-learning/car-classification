import difflib
import pickle
import numpy as np
from sklearn.linear_model import LinearRegression as LR
from sklearn.metrics import mean_squared_error
from sklearn import metrics
from sklearn import svm, preprocessing
from liblinearutil import *
from svmutil import *
from svmutil import *
from liblinearutil import *
def LinearRegression(F_Train,L_Train, F_Test,L_Test):
    regr = LR()
    regr.fit(F_Train, L_Train)
    diabetes_y_pred = regr.predict(F_Test)
    return diabetes_y_pred"
def LoadFile(Feature, Label):
    with open(Feature, 'rb') as f:
        Feature = pickle.load(f)
    with open(Label, 'rb') as f:
        Label = pickle.load(f)
    Label = list(map(lambda x:1 if x==\"Non-Sport\" else 0,Label))
    # sport is 1, non-sport is 0
    return Feature,Label


def liblinear(f_train, l_train, f_test, l_test, modelName):
    F_Train,L_Train =LoadFile(f_train,l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)
    prob = problem(L_Train, F_Train)
    param = parameter('-s 3 -c 5 -q')
    m = train(L_Train, F_Train, '-c 5')
    m = train(prob, '-w1 5 -c 5')
    m = train(prob, param)
    CV_ACC = train(L_Train, F_Train, '-v 3')
    best_C, best_rate = train(L_Train, F_Train, '-C -s 0')
    save_model(modelName, m)
    m = train(L_Train, F_Train, '-c {0} -s 0'.format(best_C)) # use the same solver: -s 0
    p_label, p_acc, p_val = predict(L_Test,F_Test, m, '-b 1')
    with open('p_label.pkl', 'wb') as f:
        pickle.dump(p_label, f)
    with open('p_acc.pkl', 'wb') as f:
        pickle.dump(p_acc, f)
    with open('p_val.pkl', 'wb') as f:
        pickle.dump(p_val, f)
    with open('best_C.pkl', 'wb') as f:
        pickle.dump(best_C, f)
    with open('best_rate.pkl', 'wb') as f:
        pickle.dump(best_rate, f)

def libsvm(f_train, l_train, f_test, l_test, modelName):
    F_Train,L_Train =LoadFile(f_train,l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)
    prob = svm_problem(L_Train, F_Train)
    param = svm_parameter('-s 3 -c 5 -h 0')
    m = svm_train(L_Train, F_Train, '-c 5')
   # m = svm_train(prob, '-t 2 -c 5')
    m = svm_train(prob, param)
    CV_ACC = svm_train(L_Train, F_Train, '-v 5')
    best_C, best_rate = train(L_Train, F_Train, '-C -s 0')
    svm_save_model(modelName, m)
    m = svm_train(L_Train, F_Train, '-c {0} -s 0'.format(best_C)) # use the same solver: -s 0
    p_labels, p_acc, p_vals = svm_predict(L_Test,F_Test, m)
    with open('p_label.pkl', 'wb') as f:
        pickle.dump(p_labels, f)
    with open('p_acc.pkl', 'wb') as f:
        pickle.dump(p_acc, f)
    with open('p_val.pkl', 'wb') as f:
        pickle.dump(p_vals, f)
    with open('best_C.pkl', 'wb') as f:
        pickle.dump(best_C, f)
    with open('best_rate.pkl', 'wb') as f:
        pickle.dump(best_rate, f)

def rbf_lib(f_train, l_train, f_test, l_test):
    F_Train,L_Train =LoadFile(f_train, l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)
    prob = svm_problem(L_Train, F_Train)
    param = svm_parameter('-s 3 -c 5 -h 0')
    m = svm_train(prob, '-t 2 -g 0.000001')
    p_labels, p_acc, p_vals = svm_predict(L_Test,F_Test, m)
    return p_acc
    
def libsvm_rbf(f_train, l_train, f_test, l_test):
    F_Train,L_Train =LoadFile(f_train,l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)
    prob = svm_problem(L_Train, F_Train)
    param = svm_parameter('-s 3 -c 5 -h 0')
    m = svm_train(L_Train, F_Train, '-c 5')
    m = svm_train(prob, '-t 2 -c 5')
    m = svm_train(prob, param)
    CV_ACC = svm_train(L_Train, F_Train, '-v 5')
    best_C, best_rate = train(L_Train, F_Train, '-C -s 0')
    svm_save_model(modelName, m)
    m = svm_train(L_Train, F_Train, '-c {0} -s 0'.format(best_C)) # use the same solver: -s 0
    p_labels, p_acc, p_vals = svm_predict(L_Test,F_Test, m)
    with open('p_label.pkl', 'wb') as f:
        pickle.dump(p_labels, f)
    with open('p_acc.pkl', 'wb') as f:
        pickle.dump(p_acc, f)
    with open('p_val.pkl', 'wb') as f:
        pickle.dump(p_vals, f)
    with open('best_C.pkl', 'wb') as f:
        pickle.dump(best_C, f)
    with open('best_rate.pkl', 'wb') as f:
        pickle.dump(best_rate, f)
        
def libsvmSklearn(f_train, l_train, f_test, l_test):
    F_Train,L_Train =LoadFile(f_train,l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)
    clf = svm.SVC()
    clf.fit(F_Train, L_Train)
    predicts = clf.predict(F_Test)
    ratio = difflib.SequenceMatcher(None, L_Test, predicts).ratio()
    return predicts, ratio
    
def classifcation(f_train, l_train, f_test, l_test, kernel):
    F_Train,L_Train =LoadFile(f_train,l_train)
    F_Test,L_Test =LoadFile(f_test, l_test)   
    clf = svm.SVC(kernel=kernel)
    clf.fit(F_Train,L_Train)
    predicts = clf.predict(F_Test)
    accuracy = difflib.SequenceMatcher(None, L_Test, predicts).ratio()
    with open('labels_predict.pkl', 'wb') as f:
        pickle.dump(predicts, f)
    with open('accuracy.pkl', 'wb') as f:
        pickle.dump(accuracy, f)
    return predicts, accuracy
    
if __name__ == '__main__':
    "#db1
    liblinear('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl', 'modelDb1')
    "#db2
    liblinear('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl', 'modelDb2')
    "#db3
    liblinear('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl', 'modelDb3')

    db1 = rbf_lib('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl')
    db2 = rbf_lib('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl')
    db3 = rbf_lib('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl')
    
    m = svm_train(L_Train, F_Train, '-c {0} -s 0'.format(best_C))
    p_labels, p_acc, p_vals = svm_predict(L_Test,F_Test, m)

    preicts, ratio = libsvmSklearn('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl')
    print(preicts)
    #db1
    libsvm('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl', 'modelDb1')
    #db2
    libsvm('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl', 'modelDb2')
    #db3
    libsvm('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl', 'modelDb3')

    with open('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\Name_Test.pkl', 'rb') as f:
        x = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\accuracy\\\\liblinear\\\\db1\\\\p_label.pkl', 'rb') as f:
        p_label = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\accuracy\\\\liblinear\\\\db1\\\\p_acc.pkl', 'rb') as f:
        p_acc = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\accuracy\\\\liblinear\\\\db1\\\\p_val.pkl', 'rb') as f:
        p_val = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\accuracy\\\\liblinear\\\\db1\\\\best_C.pkl', 'rb') as f:
        best_C = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\accuracy\\\\liblinear\\\\db1\\\\best_rate.pkl', 'rb') as f:
        best_rate = pickle.load(f)
    p_label = np.array(p_label, dtype= int)"

    # db1"
    #rbf
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl',kernel='rbf')
    print(ratio)
    print(predicts)
    #poly
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl',kernel='poly')
    print(ratio)
    print(predicts)
    #linear
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\L_Test.pkl',kernel='linear')
    print(ratio)
    print(predicts)

    # db 2
    #rbf
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl',kernel='rbf')
    print(ratio)
    print(predicts)
    #poly
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl',kernel='poly')
    print(ratio)
    print(predicts)
    #linear
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db2\\\\L_Test.pkl',kernel='linear')
    print(ratio)
    print(predicts)

    #db3:
    #rbf
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl',kernel='rbf')
    print(ratio)
    print(predicts)
    #poly
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl',kernel='poly')
    print(ratio)
    print(predicts)
    #linear
    predicts, ratio = classifcation('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Train.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Train.pkl', 'D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\F_Test.pkl','D:\\\\CNNs-car\\\\Submit\\\\db\\\\db3\\\\L_Test.pkl',kernel='linear')
    print(ratio)
    print(predicts)
    
    with open('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\Name_Train.pkl', 'rb') as f:
        Name_Train = pickle.load(f)
    print(Name_Train)

