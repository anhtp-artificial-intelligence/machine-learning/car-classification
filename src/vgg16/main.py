import numpy as np
import pickle
import random
from sklearn import svm, preprocessing
import difflib

def parseDict(dataset):
    randomSample = 730
    features = []
    names = []
    labels = []

    featureTest =[]
    nameTest = []
    labelTest = []
    
    random.shuffle(dataset)
    trainSet = dataset[:randomSample]
    testSet = dataset[randomSample:]   

    for item in trainSet:
        features.append(item['feature'])
        names.append(item['path'])
        labels.append(item['type'])

    for item in testSet:
        featureTest.append(item['feature'])
        nameTest.append(item['path'])
        labelTest.append(item['type'])

    return features, names, labels, featureTest, nameTest, labelTest
        
def saveDict(obj, pathFile):
    pickle_out = open(pathFile,"wb")
    pickle.dump(obj, pickle_out)
    pickle_out.close()

def loadDict(pathFile):
    pickle_in = open(pathFile,"rb")
    return pickle.load(pickle_in)


if __name__ == "__main__":
    features, names, labels, featureTest, nameTest, labelTest = parseDict(loadDict("vgg16_features.pickle"))

    print "this is feature train"
    print len(features)
    print len(names)
    print len(labels)    

    print "this is feature test"
    print len(featureTest)
    print len(nameTest)
    print len(labelTest)    
    
    clf = svm.SVC(kernel="rbf")
    clf.fit(features, labels)

    predicts = clf.predict(featureTest)

    print "Label test: ", labelTest
    print "Predict list: ", predicts
    print "percent: ", difflib.SequenceMatcher(None, labelTest, predicts).ratio()



