import numpy as np
import pickle
import sklearn
from sklearn.model_selection import train_test_split
import ast
from sklearn.metrics import mean_squared_error

def LoadFileFeature():
    with open('D:\\\\CNNs-car\\\\Submit\\\\Feature\\\\AlexNet\\\\lst.pkl', 'rb') as f:
        lst = pickle.load(f)
    with open('D:\\\\CNNs-car\\\\Submit\\\\ImageCLass\\\\NameImage.txt','r') as f:
        NameImages = f.readlines()
    with open('D:\\\\CNNs-car\\\\Submit\\\\ImageCLass\\\\index.txt', 'r') as f:
        labels = f.readlines()
    NameImages = np.array([str(x).replace('\\n','') for x in NameImages])
    lst = np.array([str(x).replace('\\n','') for x in lst])
    labels = np.array([str(x).replace('\\n','') for x in labels])
    return lst,NameImages,labels

def splitDataSet(train,label):
    dt = []
    n = []
    for i in range(len(train)):
        dt.append(ast.literal_eval(train[i][1]))
        n.append(train[i][0])
    return dt,n,label

def SplitData(lst, NameImages, labels,random_state):
    dic = []
    for i in range(len(NameImages)):
        dic.append([NameImages[i],lst[i]])
    X_train, X_test, y_train, y_test = train_test_split(dic, labels, test_size=0.2, random_state=random_state)
    F_Train, Name_Train, L_Train = splitDataSet(X_train,y_train)
    F_Test, Name_Test, L_Test = splitDataSet(X_test,y_test)
    return F_Train, Name_Train, L_Train,F_Test, Name_Test, L_Test

def SaveFile(F_Train, Name_Train, L_Train,F_Test, Name_Test, L_Test):
    with open('F_Train.pkl', 'wb') as fid:
        pickle.dump(F_Train, fid)
    with open('Name_Train.pkl', 'wb') as fid:
        pickle.dump(Name_Train, fid)
    with open('L_Train.pkl', 'wb') as fid:
        pickle.dump(L_Train, fid)
    with open('F_Test.pkl', 'wb') as fid:
        pickle.dump(F_Test, fid)
    with open('Name_Test.pkl', 'wb') as fid:
        pickle.dump(Name_Test, fid)
    with open('L_Test.pkl', 'wb') as fid:
        pickle.dump(L_Test, fid)
        
if __name == '__main__': 
    lst,NameImage,labels = LoadFileFeature()
    F_Train, Name_Train, L_Train,F_Test, Name_Test, L_Test = SplitData(lst,NameImage,labels,random_state=200)
    SaveFile(F_Train, Name_Train, L_Train,F_Test, Name_Test, L_Test)
    with open('D:\\\\CNNs-car\\\\Submit\\\\db\\\\db1\\\\Name_Train.pkl', 'rb') as f:
        x = pickle.load(f)
