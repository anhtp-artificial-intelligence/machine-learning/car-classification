import os,sys
import csv    
import numpy
import pandas as pd
from random import randint

def loadImage(strDirect):
    list_image = []    
    list_path = []    
    print strDirect    
    for filename in glob.glob(strDirect): #assuming gif
        img = cv2.imread(filename)    
        drive, path = os.path.splitdrive(filename)    
        path, filename = os.path.split(path)    
        resized_image = cv2.resize(img, (128, 128))     
        gray_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)    
        list_image.append(resized_image)    
        list_path.append(filename)    
    return list_image, list_path

def loadDataset(data, PerOfTrain = 0.8):    
    raw_data = open(data, 'rt')    
    reader = csv.reader(raw_data, delimiter=',', quoting=csv.QUOTE_NONE)    
    x = list(reader)    
    data = numpy.array(x).astype('float')

if __name__ == "__main__":
    Sport,list_path_Sport = loadImage('C:\\Users\\Lenovo\\Downloads\\Car\\Sport/*.jpg')    
    Non_Sport, list_path_NonSport=loadImage('C:\\Users\\Lenovo\\Downloads\\Car\\Non-Sport/*.jpg')
    dataset = []    
    # 1 sport    
    # 0 nonSport    
    numSport = len(Sport)    
    numNonSport = len(Non_Sport)    
    index = 0;    
    for i in range(numSport+numNonSport):    
        value = randint(0,100)%2    
        select = 0    
        if(numSport == 0):    
            dataset.append({'label': 0,'dataset': Non_Sport[numNonSport-1], 'path' : list_path_NonSport[numNonSport-1]})    
            numNonSport = numNonSport -1    
        elif numNonSport==0:    
            dataset.append({'label': 1, 'dataset': Sport[numSport-1], 'path': list_path_Sport[numSport-1]})    
            numSport = numSport -1    
        else :    
            if value == 1 :    
                dataset.append({'label': 1, 'dataset': Sport[numSport-1], 'path' : list_path_Sport[numSport-1]})    
                numSport = numSport - 1    
            else :    
                dataset.append({'label': 0, 'dataset':Non_Sport[numNonSport-1], 'path' : list_path_NonSport[numNonSport-1]})    
                numNonSport = numNonSport - 1    
        index = index +1
    df = pd.DataFrame.from_dict(dataset)    
    df.to_pickle('D:\\\\report\\\\datasetCar')
    df.head()