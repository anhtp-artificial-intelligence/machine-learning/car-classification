link dataset (preprocessed): https://drive.google.com/drive/folders/0BzqidGHhJQbFZHlEc3lKZmxrcTQ?usp=sharing
Annotation from dataset: https://drive.google.com/drive/folders/0BzqidGHhJQbFcnpHUkdUN010X1U (use "datasetCar" file) -> then use the code 
"https://gitlab.com/anhtp/car-classification/blob/develop/tools/CreateData.ipynb" to load annotation file.